# Credentify tags to ESCO skills

Contains a mapping of pre-determined credentify tags to ESCO skill categories in JSON form.
Each entry in the file `the_mapping.csv` should be in the form of a row
```csv
Skill | ESCO category | ESCO sub-category
```
The symbol `|` functions as a column separator.
